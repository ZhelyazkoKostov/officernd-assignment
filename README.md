# **INSTRUCTIONS TO ACCESS AND RUN TESTS**

### **Test Cases Link** *(https://docs.google.com/spreadsheets/d/1XKJvxD0fwZcYd82dHxOsLQMRGKahserUDd2j9Ljbtbw/edit?usp=sharing)*


### **Instructions for Bonus Asignment Automation Test**

1.  Navigate to Folder "Postman";
2.  Pull localy "Postman" Folder;
3.  Have Postman Installed;
4.  Run RunMembershipTest.bat File;
5.  Access Reports in newly created newman folder